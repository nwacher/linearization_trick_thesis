# Un truco de linearización

Este repositorio contiene el trabajo de tesina desarrollado como parte del programa de Maestria en Matematicas
cursado en la UNAM y dirigido por el Dr Francisco Torres Ayala.

Se desarrollan y contrastan dos técnicas desarrolladas por Haagerup, Schultz y Thorbjornsen, ambas conocidas en
la literatura como 'Truco de linearización', que resultan funndamentales al momento de demostrar que el funtor
Ext de cierta familia relevante de álgebras C* no tiene estructura de grupo sin la necesidad de recurrir a la
K-Teoría o a la geometría no conmutativa. Estas técnicas a su vez conectan a la teoría de álgebras C* y su clasificación
con la Teoría de Matrices Aleatorias.
